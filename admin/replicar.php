<a data-toggle='modal' data-target='#replicar_<?=$table_em_uso?>' class="btn btn-success icone aright m-right btn-top" ><i class="zmdi zmdi-collection-plus"></i> REPLICAR</a>

<!-- BUSCA -->
<div id="replicar_<?=$table_em_uso?>" class="buscar modal fade" role="dialog" data-backdrop="static">

	<div class="modal-dialog">
		<div class="panel panel-primary">
			<div class="panel-heading">REPLICAR<a data-dismiss="modal" class="fechar"><i class="glyphicon glyphicon-remove"></i></a></div>
			<div class="panel-body">

				<form action="tabela_acoes.php" class="form-replicar" method="POST" enctype="multipart/form-data">
					<div class="row">

						<div class="col-md-12">

							<label>
								<span>Replicar:</span>
								<select class="form-control" name="replicar">
								<?
								if(possui_ordem($table_em_uso)) { $oby = "admin_ordem"; } else { $oby = "id"; }
								$sql = mysqli_query($GLOBALS["db"], "SELECT * FROM $table_em_uso ORDER BY $oby ASC");
								if(mysqli_num_rows($sql) != 0) {
									while($dados = mysqli_fetch_array($sql)) {
									?>
									<option value="<?=$dados["id"]?>"><?=pega_campo_principal($table_em_uso,$dados["id"])?></option>
									<?
									}
								} ?>
								</select>
							</label>
							<hr />
							<button type="button" onclick="$(this).closest('form').submit();" class="btn-buscar btn btn-success aright icone" /><i class="zmdi zmdi-collection-plus"></i> REPLICAR</button>

						</div>

					</div>
					<input type="hidden" name="table" value="<?=$table_em_uso?>" />
					<input type="hidden" name="table_original" value="<?=$_GET["t"]?>" />
					<input type="hidden" name="id" value="<?=$_GET["id"]?>" />
					<input type="hidden" name="acao" value="replicar" />
				</form>

			</div>
		</div>
	</div>
</div>