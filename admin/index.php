


<? include("header.php"); ?>
<? require_once("funcoes.php"); ?>




<?
if(!file_exists("config.php")) {
	require("setup.php");
} else {

	require_once("config.php");

	if(!isset($_SESSION["id_usuario"])) {
		header("Location: login.php");
		exit;
	}

	if(isset($_GET["nivel"])) { $nivel = $_GET["nivel"]; } else { $nivel = '0'; }

	if(!isset($_SESSION['nivel'][$nivel])) { $_SESSION['nivel'][$nivel] = ""; }
	if(!isset($_SESSION['bread'][$nivel])) { $_SESSION['bread'][$nivel] = ""; }

	?>
	<input type="hidden" id="actual_url" value="<?=$REQUEST_PROTOCOL."://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>">
	<div class="container-fluid">
		<div class="row">
			<div class="top-mob">
				<img src="img/logo.png">
			</div>
			<div class="menu-mobile"></div>
			<div class="espace"></div>

			<div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">

				<div class="menu_container">
					<?
					$sql = "SHOW full TABLES FROM $dbname";
					$result = mysqli_query($GLOBALS["db"],$sql);
					?>
					<div class="logo"><img src="../img/logo_admin.png"></div>

					<ul class="menu">

						<?
						$menu = array();
						while($tables = mysqli_fetch_array($result)) {
							$nome = normalizaString($tables[0]);

							$editavel = 0;

							$sql_config = mysqli_query($GLOBALS["db"], "SELECT * FROM admin_configuracoes WHERE tabela = '".$tables[0]."' and campo = ''");
							if(mysqli_num_rows($sql_config) != 0) {
								$config = mysqli_fetch_array($sql_config);
								$editavel = $config["editavel"];
								if($config["label"] != "") { $nome = $config["label"]; }
							}

							$html = "";
							if($editavel) {
								if(tem_permissao($tables[0],'acessar')) {
									$html = "<li><a href='index.php?pag=tabela&t=".$tables[0]."'>";
									if($config["icone"] != "") { $html .= '<i class="'.$config['icone'].'"></i>'; }
									$html .= "<span>".$nome."</span></a></li>";
								}
							}

							if(strpos($nome,"_") !== false) {
								$tb_nome = explode("_",$nome);
								$key = $tb_nome[0];
								if(isset($menu[$key])) {
									$menu[$key] .= $html;
								} else {
									$menu[$key] = $html;
								}
							} else {
								if(isset($menu['restante'])) {
									$menu['restante'] .= $html;
								} else {
									$menu['restante'] = $html;
								}
							}



						}

						foreach($menu as $m=>$v) {
							if($m != 'restante') {
								echo "<li style='border-bottom: 3px solid #fff; padding-top: 10px;padding-bottom: 2px; text-transform: uppercase; background: #003a6b;color: #fff;padding-left: 5px;font-size: 16px;font-weight: bold;margin-top: 5px;''>".$m."</li>";
								echo str_replace($m."_","",$v);
							} else {
								echo "<li style='border-bottom: 3px solid #fff; padding-top: 10px; padding-bottom: 2px; text-transform: uppercase; background: #003a6b;color: #fff;padding-left: 5px;font-size: 16px;font-weight: bold;margin-top: 5px;'>Geral</li>";
								echo $v;
							}
						}

						?>

						<? if($_SESSION["tipo_usuario"] == 1) { ?>
							<li style="margin-top: 15px;"><a href="index.php?pag=configuracoes"><i class="glyphicon glyphicon-cog"></i><span>Configurações</span></a></li>
						<? } ?>

						<li style="margin-top: 15px;"><a href="index.php?pag=logout" onclick="sairScript()"><i class="glyphicon glyphicon-log-out"></i><span>Sair</span></a></li>

					</ul>
				</div>
			</div>

			<div class="col-xs-offset-1 col-sm-offset-0 col-xs-11 col-md-offset-0 col-sm-8 col-md-9 col-lg-10 columns ">
				<?
				if(@$_GET["pag"] != "") {
					$pag = $_GET["pag"];
				} else {
					$pag = "home";
				}

				include($pag.".php");
				?>
			</div>

		</div>
	</div>
	<?
}
?>

<script>
function sairScript() {
  alert("Volte sempre!!");
}
</script>

<? include("footer.php"); ?>