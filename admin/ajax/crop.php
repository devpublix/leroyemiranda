<?php
	require_once("../verifica_login.php");
	require_once("../config.php");
	require_once("../php/canvas.php");

	$x1 = $_POST["x1"];
	$y1 = $_POST["y1"];
	$x2 = $_POST["x2"];
	$y2 = $_POST["y2"];
	$w = $_POST["w"];
	$h = $_POST["h"];

	$nome_foto = $_POST["crop_imgsrc"];

	$dir = "../uploads/";
	$file = $dir.$nome_foto;
	$t = getimagesize($file);

	$w2 = $t[0]; $h2 = $t[1];
	
	$canvas = new canvas();
	$canvas->load_url($file)->set_crop_coordinates($x1, $y1, $x2, $y2)->resize($w, $h, 'crop')->resize($w, $h)->save($dir.$nome_foto);

	$canvas->load_url($file)->resize( 100 )->save($dir."/p_".$nome_foto);

?>