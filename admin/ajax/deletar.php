<?php
	require_once("../verifica_login.php");
	require_once("../config.php");
	require_once("../funcoes.php");

	$table = $_POST["table"];
	$id = $_POST["id"];

	//Verifica se tem imagem ou arquivo
	$sql = mysqli_query($GLOBALS["db"], "SELECT campo FROM admin_configuracoes WHERE tabela = '$table' and (tipo = 'arquivo' or tipo = 'imagem')");
	if(mysqli_num_rows($sql) != 0) {

		$campo = mysqli_result($sql,0,"campo");
		$sql = mysqli_query($GLOBALS["db"], "SELECT $campo FROM $table WHERE id = '$id'");
		if(mysqli_num_rows($sql) != 0) {
			$img = mysqli_result($sql,0,$campo);

			if(mysqli_query($GLOBALS["db"], "UPDATE $table SET $campo = '' WHERE id = '$id'")) {
				@unlink("../uploads/".$img);
				@unlink("../uploads/p_".$img);
				@unlink("../uploads/g_".$img);
				@unlink("../uploads/m_".$img);
			}

		}
	}

	if(possui_ordem($table)) {
		//Elimina buraco deixado pela saída do elemento original
		$get_ordem = mysqli_query($GLOBALS["db"], "SELECT admin_ordem FROM $table WHERE id = '$id'");
		$ordem = mysqli_result($get_ordem,0,"admin_ordem");
		mysqli_query($GLOBALS["db"], "UPDATE $table SET admin_ordem = admin_ordem-1 WHERE admin_ordem > $ordem");
	}

	if(mysqli_query($GLOBALS["db"], "DELETE FROM $table WHERE id = '$id'")) {

		//Deleta da galeria se tiver
		$get_galeria = mysqli_query($GLOBALS["db"], "SELECT * FROM admin_galerias WHERE tabela = '$table' and id_externo = '$id'") or die(mysqli_error($GLOBALS["db"]));
		if(mysqli_num_rows($get_galeria) != 0) {
			while($gal = mysqli_fetch_array($get_galeria)) {
				$img = $gal["caminho"];
				@unlink("../uploads/".$img);
				@unlink("../uploads/p_".$img);
				@unlink("../uploads/g_".$img);
				@unlink("../uploads/m_".$img);
			}
			mysqli_query($GLOBALS["db"], "DELETE FROM admin_galerias WHERE tabela = '$table' and id_externo = '$id'");
		}

		//Verifica se há registros em outras tabelas
		$externo_sql = mysqli_query($GLOBALS["db"], "select table_name from information_schema.columns where table_schema = '$dbname' and column_name = 'id_".$table."'") or die(mysqli_error($GLOBALS["db"]));
		if(mysqli_num_rows($externo_sql) != 0) {
			while($externo = mysqli_fetch_array($externo_sql)) {

				//Verifica se tem imagem ou arquivo
				$sql_check = mysqli_query($GLOBALS["db"], "SELECT campo FROM admin_configuracoes WHERE tabela = '".$externo["table_name"]."' and (tipo = 'arquivo' or tipo = 'imagem')") or die(mysqli_error($GLOBALS["db"]));
				if(mysqli_num_rows($sql_check) != 0) {

					$campo2 = mysqli_result($sql_check,0,"campo");
					$sql_campo = mysqli_query($GLOBALS["db"], "SELECT ".$campo2." FROM ".$externo["table_name"]." WHERE id_".$table." = '".$id."'") or die("erro:".mysqli_error($GLOBALS["db"]));
					if(mysqli_num_rows($sql_campo) != 0) {
						while($cmp = mysqli_fetch_array($sql_campo)) {
							$img = $cmp[$campo2];
							@unlink("../uploads/".$img);
							@unlink("../uploads/p_".$img);
							@unlink("../uploads/g_".$img);
							@unlink("../uploads/m_".$img);
						}
					}

				}

				mysqli_query($GLOBALS["db"], "DELETE FROM ".$externo["table_name"]." WHERE id_".$table." = '$id'");

			}
		}
	}


?>