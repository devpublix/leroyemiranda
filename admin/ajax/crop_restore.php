<?php
	require_once("../verifica_login.php");
	require_once("../config.php");
	require_once("../php/canvas.php");

	$x1 = $_POST["x1"];
	$y1 = $_POST["y1"];
	$x2 = $_POST["x2"];
	$y2 = $_POST["y2"];
	$w = $_POST["w"];
	$h = $_POST["h"];

	$nome_foto = $_POST["crop_imgsrc"];

	$dir = "../uploads/";
	$file = $dir.$nome_foto;
	
	@unlink($file);
	@unlink($dir."p_".$nome_foto);

	$grande = $dir."g_".$nome_foto;

	copy($grande,$file);

	$canvas->load_url($file)->resize( 100 )->save($dir."/p_".$nome_foto);

?>