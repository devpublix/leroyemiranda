<?php

	$tipos_de_campos = array(
		"text",
		"textarea",
		"tinymce",
		"numero",
		"cpf",
		"cnpj",
		"cep",
		"estado",
		"cidade",
		"email",
		"imagem",
		"arquivo",
		"select",
		"radio",
		"checkbox",
		"telefone",
		"dinheiro",
		"senha",
		"data",
		"datahora",
		"cor",
		"galeria",
		"tabelas_banco"
	);

?>