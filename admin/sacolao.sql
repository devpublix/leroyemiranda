-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 25-Out-2015 às 21:52
-- Versão do servidor: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sacolao`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `admin_configuracoes`
--

CREATE TABLE IF NOT EXISTS `admin_configuracoes` (
`id` int(11) NOT NULL,
  `tabela` varchar(250) NOT NULL,
  `campo` varchar(250) NOT NULL,
  `editavel` tinyint(4) NOT NULL,
  `obrigatorio` tinyint(4) NOT NULL,
  `visivel` tinyint(4) NOT NULL,
  `pesquisavel` tinyint(4) NOT NULL,
  `tamanho` int(2) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `configuracao` text NOT NULL,
  `orderby` text NOT NULL,
  `label` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `admin_configuracoes`
--

INSERT INTO `admin_configuracoes` (`id`, `tabela`, `campo`, `editavel`, `obrigatorio`, `visivel`, `pesquisavel`, `tamanho`, `tipo`, `configuracao`, `orderby`, `label`) VALUES
(1, 'admin_configuracoes', '', 0, 0, 0, 0, 0, '', '', '', ''),
(2, 'telefones', '', 0, 0, 0, 0, 0, '', '', '', ''),
(3, 'usuário', '', 0, 0, 0, 0, 0, '', '', '', 'Usuário'),
(4, 'exemplo', '', 1, 0, 0, 0, 0, '', '', 'admin_ordem ASC', 'Exemplo2'),
(5, 'ponto_comercial', '', 0, 0, 0, 0, 0, '', '', 'id ASC', ''),
(6, 'exemplo', 'text', 1, 0, 1, 1, 12, 'text', '', '', 'Text'),
(7, 'exemplo', 'textarea', 1, 0, 1, 1, 12, 'textarea', '', '', ''),
(8, 'exemplo', 'id', 0, 0, 0, 0, 6, 'numero', '', '', ''),
(9, 'exemplo', 'cpf', 1, 0, 1, 1, 6, 'cpf', '', '', ''),
(10, 'exemplo', 'cnpj', 1, 0, 0, 0, 6, 'cnpj', '', '', ''),
(11, 'exemplo', 'tinymce', 1, 0, 1, 1, 0, 'tinymce', '', '', 'Editor básico'),
(12, 'exemplo', 'cep', 1, 0, 0, 0, 0, 'cep', '', '', ''),
(13, 'exemplo', 'estado', 1, 0, 0, 0, 0, 'estado', '', '', ''),
(14, 'exemplo', 'cidade', 1, 0, 0, 0, 0, 'cidade', '', '', ''),
(15, 'exemplo', 'email', 1, 0, 0, 0, 0, 'email', '', '', ''),
(16, 'exemplo', 'arquivo', 1, 0, 0, 0, 0, 'arquivo', 'jpg,jpeg,png,gif', '', ''),
(17, 'exemplo', 'imagem', 1, 0, 0, 0, 0, 'imagem', '500|100|jpg,jpeg,png,gif', '', ''),
(18, 'exemplo', 'selecao', 1, 0, 0, 0, 0, 'select', 'Opt1&Opt2&Opt3', '', ''),
(19, 'exemplo', 'checkbox', 1, 0, 0, 0, 0, 'checkbox', 'Opt1&Opt2&Opt3', '', ''),
(20, 'exemplo', 'radio', 1, 0, 0, 0, 0, 'radio', 'Opt1&Opt2&Opt3', '', ''),
(21, 'exemplo', 'dinheiro', 1, 0, 0, 0, 0, 'dinheiro', '', '', ''),
(22, 'exemplo', 'telefone', 1, 0, 0, 0, 0, 'telefone', '', '', ''),
(23, 'exemplo', 'senha', 1, 0, 0, 0, 0, 'senha', '', '', ''),
(24, 'exemplo', 'data', 1, 0, 0, 0, 0, 'data', '', '', ''),
(25, 'exemplo', 'numero', 1, 0, 0, 0, 0, 'numero', '', '', ''),
(26, 'exemplo', 'datahora', 1, 0, 0, 0, 0, 'datahora', '', '', ''),
(27, 'exemplo', 'cor', 1, 0, 0, 0, 0, 'cor', '', '', ''),
(28, '<br />\n<b>Notice</b>:  Undefined variable: table in <b>D:Trabalhoadminconfiguracoes.php</b> on line <b>30</b><br />\n', '<br />\n<b>Notice</b>:  Undefined variable: field in <b>D:Trabalhoadminconfiguracoes.php</b> on line <b>30</b><br />\n', 0, 0, 0, 0, 0, '', '', '', 'Exemplo'),
(29, '<br />\n<b>Notice</b>:  Undefined variable: table in <b>D:Trabalhoadminconfiguracoes.php</b> on line <b>30</b><br />\n', '', 0, 0, 0, 0, 0, '', '', '', 'Exemplo2'),
(30, '<br />\n<b>Notice</b>:  Undefined variable: table in <b>D:Trabalhoadminconfiguracoes.php</b> on line <b>35</b><br />\n', '', 0, 0, 0, 0, 0, '', '', '', 'Exemplo2'),
(31, 'telefones', 'id', 0, 0, 0, 0, 0, '', '', '', ''),
(32, 'telefones', 'tipo', 1, 0, 0, 1, 0, '', '', '', ''),
(33, 'telefones', 'id_exemplo', 0, 0, 0, 0, 0, '', '', '', ''),
(34, 'telefones', 'número_do_telefone', 1, 0, 0, 1, 0, 'telefone', '', '', ''),
(35, 'telefones', 'ddd', 1, 0, 0, 1, 0, '', '', '', ''),
(36, 'telefones', 'id_ponto_comercial', 0, 0, 0, 0, 0, '', '', '', ''),
(37, 'telefones', 'whatsapp', 1, 0, 0, 0, 0, '', '', '', ''),
(38, 'telefones', 'operadora', 1, 0, 0, 0, 0, '', '', '', ''),
(39, 'ponto_comercial', 'Nome_fantasia', 1, 0, 0, 1, 0, '', '', '', ''),
(40, 'ponto_comercial', 'Logo', 1, 0, 0, 0, 0, 'imagem', '300|200|jpg,jpeg,png,gif', '', ''),
(41, 'ponto_comercial', 'Razao_social', 0, 0, 0, 1, 0, '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `exemplo`
--

CREATE TABLE IF NOT EXISTS `exemplo` (
`id` int(11) NOT NULL COMMENT '0;0;12;number|teste',
  `text` varchar(250) NOT NULL COMMENT '1;1;12;text;Campo Text',
  `textarea` text NOT NULL COMMENT '1;0;12;textarea;Área de Texto',
  `tinymce` text NOT NULL COMMENT '1;0;12;tinymce;Editor de Texto',
  `cpf` varchar(25) DEFAULT NULL COMMENT '1;1;12;cpf;CPF3',
  `cnpj` varchar(25) NOT NULL COMMENT '1;0;12;cnpj;CNPJ',
  `cep` varchar(10) NOT NULL COMMENT '1;0;12;cep;CEP',
  `estado` varchar(100) NOT NULL COMMENT '1;0;12;estado;Estado',
  `cidade` varchar(100) DEFAULT NULL COMMENT '1;0;12;cidade|;Cidade5',
  `email` varchar(250) NOT NULL COMMENT '1;0;12;email;E-mail',
  `imagem` text NOT NULL COMMENT '1;0;12;imagem|300x300;Imagem',
  `arquivo` text NOT NULL COMMENT '1;0;12;arquivo;Arquivo',
  `selecao` varchar(250) DEFAULT NULL COMMENT '1;0;12;select|ponto_comercial|Cidade|Cidade;Select2',
  `radio` varchar(250) NOT NULL COMMENT '1;0;12;radio|Opt1&Opt2&Opt3;Radio',
  `checkbox` text NOT NULL COMMENT '1;1;12;checkbox|Opt1&Opt2&Opt3;Checkbox',
  `telefone` varchar(15) NOT NULL COMMENT '1;0;12;telefone;Telefone',
  `dinheiro` float NOT NULL COMMENT '1;0;12;dinheiro;Dinheiro',
  `senha` varchar(250) NOT NULL COMMENT '1;0;12;senha;Senha',
  `numero` int(11) NOT NULL COMMENT '1;0;12;numero;Número',
  `data` date NOT NULL COMMENT '1;0;12;data;Data',
  `datahora` datetime NOT NULL COMMENT '1;0;12;datahora;Data e Hora',
  `cor` varchar(15) NOT NULL COMMENT '1;0;12;cor;Cor',
  `admin_ordem` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1 COMMENT='0';

--
-- Extraindo dados da tabela `exemplo`
--

INSERT INTO `exemplo` (`id`, `text`, `textarea`, `tinymce`, `cpf`, `cnpj`, `cep`, `estado`, `cidade`, `email`, `imagem`, `arquivo`, `selecao`, `radio`, `checkbox`, `telefone`, `dinheiro`, `senha`, `numero`, `data`, `datahora`, `cor`, `admin_ordem`) VALUES
(29, 'aaa', '', '', '', '', '', '', NULL, '', '', '', '', '', '', '', 0, '', 0, '0000-00-00', '0000-00-00 00:00:00', '', 11),
(30, 'cassac', '', '', '', '', '', '', NULL, '', '', '', '', '', '', '', 0, '', 0, '0000-00-00', '0000-00-00 00:00:00', '', 14),
(31, 'bbb', '', '', '', '', '', '', NULL, '', '', '', '', '', '', '', 0, '', 0, '0000-00-00', '0000-00-00 00:00:00', '', 12),
(32, 'ccc', '', '', '', '', '', '', NULL, '', '', '', '', '', '', '', 0, '', 0, '0000-00-00', '0000-00-00 00:00:00', '', 6),
(33, 'ddd', '', '', '', '', '', '', NULL, '', '', '', '', '', '', '', 0, '', 0, '0000-00-00', '0000-00-00 00:00:00', '', 9),
(34, 'eee', '', '', '', '', '', '', NULL, '', '', '', '', '', '', '', 0, '', 0, '0000-00-00', '0000-00-00 00:00:00', '', 10),
(35, 'fff', '', '', '', '', '', '', NULL, '', '', '', '', '', '', '', 0, '', 0, '0000-00-00', '0000-00-00 00:00:00', '', 13),
(36, 'ggg', '', '', '', '', '', '', NULL, '', '', '', '', '', '', '', 0, '', 0, '0000-00-00', '0000-00-00 00:00:00', '', 7),
(38, 'iii', '', '', '', '', '', '', NULL, '', '', '', '', '', '', '', 0, '', 0, '0000-00-00', '0000-00-00 00:00:00', '', 8),
(39, 'jjj', '', '', '', '', '', '', NULL, '', '', '', '', '', '', '', 0, '', 0, '0000-00-00', '0000-00-00 00:00:00', '', 15);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ponto_comercial`
--

CREATE TABLE IF NOT EXISTS `ponto_comercial` (
`id` int(11) NOT NULL COMMENT '0;1;6;number',
  `id_exemplo` int(11) NOT NULL,
  `Nome_fantasia` varchar(250) NOT NULL COMMENT '1;1;6;text',
  `Razao_social` varchar(250) NOT NULL COMMENT '1;1;6;text',
  `CNPJ` varchar(30) NOT NULL COMMENT '1;1;6;cnpj',
  `CEP` varchar(10) NOT NULL COMMENT '1;1;6;cep',
  `Endereço` text NOT NULL COMMENT '1;1;6;text',
  `Estado` varchar(150) NOT NULL COMMENT '1;1;6;estado',
  `Cidade` varchar(150) NOT NULL COMMENT '1;1;6;cidade',
  `Contato` varchar(250) NOT NULL COMMENT '1;1;6;text',
  `Endereço_web` text NOT NULL COMMENT '1;1;6;text',
  `Endereço_facebook` text NOT NULL COMMENT '1;1;6;text',
  `Email` varchar(250) NOT NULL COMMENT '1;1;6;email',
  `Logo` text NOT NULL COMMENT '1;1;6;imagem|300x300',
  `Rede` int(11) NOT NULL COMMENT '1;1;6;select|redes',
  `Data_cadastro` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '0;1;6;datahora'
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1 COMMENT='1';

--
-- Extraindo dados da tabela `ponto_comercial`
--

INSERT INTO `ponto_comercial` (`id`, `id_exemplo`, `Nome_fantasia`, `Razao_social`, `CNPJ`, `CEP`, `Endereço`, `Estado`, `Cidade`, `Contato`, `Endereço_web`, `Endereço_facebook`, `Email`, `Logo`, `Rede`, `Data_cadastro`) VALUES
(2, 1, '', '', '', '', '', '', '', '', '', '', '', 'tulips.jpg', 0, '2015-09-24 02:21:25'),
(5, 4, '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(6, 5, '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(8, 11, '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(12, 15, '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(14, 18, '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(15, 19, '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(16, 20, '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(17, 21, '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(18, 22, '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(25, 29, '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(26, 30, '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(27, 31, '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(28, 32, '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(29, 33, '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(30, 34, '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(31, 35, '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(32, 36, '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(34, 38, '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(35, 39, '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `telefones`
--

CREATE TABLE IF NOT EXISTS `telefones` (
`id` int(11) NOT NULL COMMENT '0;1;number',
  `id_ponto_comercial` int(11) NOT NULL COMMENT '0;2',
  `id_exemplo` int(11) NOT NULL,
  `número_do_telefone` varchar(15) NOT NULL COMMENT '1;1;6;telefone',
  `ddd` varchar(10) NOT NULL COMMENT '1;1;6;ddd',
  `tipo` varchar(10) NOT NULL COMMENT '1;1;6;radio|Fixo&Celular',
  `whatsapp` tinyint(4) NOT NULL COMMENT '1;1;6;checkbox',
  `operadora` int(11) NOT NULL COMMENT '1;1;6;select|operadoras_de_telefonia'
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1 COMMENT='0';

--
-- Extraindo dados da tabela `telefones`
--

INSERT INTO `telefones` (`id`, `id_ponto_comercial`, `id_exemplo`, `número_do_telefone`, `ddd`, `tipo`, `whatsapp`, `operadora`) VALUES
(17, 0, 1, '(11) 11111-1111', '22', '2', 0, 0),
(18, 0, 1, '(22) 22222-2222', 'feafae', '', 0, 0),
(19, 0, 1, '(33) 33333-3333', '', '', 0, 0),
(22, 0, 4, '', '', '', 0, 0),
(26, 0, 10, '', '', '', 0, 0),
(27, 0, 11, '', '', '', 0, 0),
(31, 0, 15, '', '', '', 0, 0),
(34, 0, 18, '', '', '', 0, 0),
(36, 0, 5, '', '', '', 0, 0),
(38, 0, 19, '', '', '', 0, 0),
(39, 0, 20, '', '', '', 0, 0),
(40, 0, 21, '', '', '', 0, 0),
(41, 0, 22, '', '', '', 0, 0),
(48, 0, 29, '', '', '', 0, 0),
(49, 0, 30, '', '', '', 0, 0),
(50, 0, 31, '', '', '', 0, 0),
(51, 0, 32, '', '', '', 0, 0),
(52, 0, 33, '', '', '', 0, 0),
(53, 0, 34, '', '', '', 0, 0),
(54, 0, 35, '', '', '', 0, 0),
(55, 0, 36, '', '', '', 0, 0),
(57, 0, 38, '', '', '', 0, 0),
(58, 0, 39, '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuário`
--

CREATE TABLE IF NOT EXISTS `usuário` (
`id` int(11) NOT NULL COMMENT '0;0;6;number',
  `Nome` varchar(250) NOT NULL COMMENT '1;1;6;text',
  `Email` varchar(250) NOT NULL COMMENT '1;1;6;email',
  `Senha` varchar(250) NOT NULL COMMENT '0;0;6;password',
  `Data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '0;0;6;datetime'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='1';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_configuracoes`
--
ALTER TABLE `admin_configuracoes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exemplo`
--
ALTER TABLE `exemplo`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ponto_comercial`
--
ALTER TABLE `ponto_comercial`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `telefones`
--
ALTER TABLE `telefones`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuário`
--
ALTER TABLE `usuário`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_configuracoes`
--
ALTER TABLE `admin_configuracoes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `exemplo`
--
ALTER TABLE `exemplo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '0;0;12;number|teste',AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `ponto_comercial`
--
ALTER TABLE `ponto_comercial`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '0;1;6;number',AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `telefones`
--
ALTER TABLE `telefones`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '0;1;number',AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `usuário`
--
ALTER TABLE `usuário`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '0;0;6;number';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
