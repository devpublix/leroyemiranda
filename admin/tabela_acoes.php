<?
require_once("config.php");
require_once("verifica_login.php");
include("funcoes.php");
require_once "php/canvas.php";

if(isset($_POST["table"])) {
	$table = $_POST["table"];
}

if($_POST['acao']=="atualizar_ordem"){
	$ids = explode(",", $_POST['ids']);
	$table = $_POST["tabela"];
	$campo = $_POST["campo"];
	for($i=0; $i<count($ids); $i++){
		$sql = "UPDATE admin_galerias SET ordem='".$i."' WHERE tabela='$table' and campo='$campo' and id=".$ids[$i]."";
		$query = mysqli_query($GLOBALS["db"],$sql);
	}
}

if($_POST['acao']=="registrar") {
	$sucesso = false;
	$id = "";
	if(isset($_POST["id"]) and $_POST["id"] != "") { $id = $_POST["id"]; }

	//Obtém colunas da tabela
	$sql_colunas = "SHOW COLUMNS FROM $table;";
	$query_colunas = mysqli_query($GLOBALS["db"],$sql_colunas);
	$dados_colunas = mysqli_fetch_array($query_colunas);

	$colunas = array();
	while($dados_colunas = mysqli_fetch_array($query_colunas))
	{
		$colunas[] = $dados_colunas['Field'];
	}

	//Forma a query
	$sql = forma_query($table,$colunas,$id);
	$insert = false;

	if($sql != "") {
		if(mysqli_query($GLOBALS["db"],$sql)) {
			$sucesso = true;

			if($id == "") {
				$insert = true;
				$id = mysqli_insert_id($GLOBALS["db"]);

				//ordem
				if(possui_ordem($table)) {
					$checa_vazio = mysqli_query($GLOBALS["db"], "SELECT COUNT(id) as qtd FROM $table");
					if(mysqli_result($checa_vazio,0,"qtd") != 1) {
						$get_max = mysqli_query($GLOBALS["db"], "SELECT MAX(admin_ordem) as max FROM $table");
						$max = mysqli_result($get_max,0,"max")+1;
						mysqli_query($GLOBALS["db"], "UPDATE $table SET admin_ordem = $max WHERE id = '$id'") or die(mysqli_error($GLOBALS["db"]));
					}

				}
			}
		} else {
			die(mysqli_error($GLOBALS["db"]).$sql);
		}
	} else {
		$sucesso = true;
	}

	//GALERIAS - UPLOAD E INSERT
	foreach ($_FILES as $key => $value) {

		if(in_array($key,$colunas)) {

			$ultima_ordem_galeria_sql = mysqli_query($GLOBALS["db"],"SELECT ordem FROM admin_galerias WHERE tabela = '$table' and campo = '$key' ORDER BY ordem DESC");
			$ultima_ordem = 0;
			if(mysqli_num_rows($ultima_ordem_galeria_sql) != 0) {
				$ultima_ordem = mysqli_result($ultima_ordem_galeria_sql,0,"ordem")+1;
			}

			if(is_array($value['name'])) { //Se for uma galeria...

				$total_files = sizeof($value["name"]);

				for($i = 0; $i < $total_files; $i++) {
					$ordem_atual = $ultima_ordem + $i;
					if($value['name'][$i]!="") {

						$dir = "uploads/";
						$nome = $value['name'][$i];
						$nome_arr = explode(".", $nome);
						$ext = ".".end($nome_arr);
						$nome_original = cleanForShortURL($nome_arr[0]);
						$nome_foto = $nome_original.$ext;
						$j = 0;
						while(file_exists($dir.$nome_foto)){
							$j++;
							$nome_foto = $nome_original."_".$j.$ext;
						}

						move_uploaded_file($value['tmp_name'][$i], "$dir$nome_foto");

						if(strpos($value['type'][$i], "image")!==false){

							$file = $dir.$nome_foto;
							$t = getimagesize($file);

							if($t[0]>1000){
								$canvas = new canvas();
								$canvas->load_url($file)->resize( 1000 )->save($dir."/g_".$nome_foto);
							} else {
								$canvas = new canvas();
								$canvas->load_url($file)->resize( "100%" )->save($dir."/g_".$nome_foto);
							}

							if($t[0]>500){
								$canvas = new canvas();
								$canvas->load_url($file)->resize( 500 )->save($dir."/m_".$nome_foto);
							} else {
								$canvas = new canvas();
								$canvas->load_url($file)->resize( "100%" )->save($dir."/m_".$nome_foto);
							}

							if($t[0]>100){
								$canvas = new canvas();
								$canvas->load_url($file)->resize( 100 )->save($dir."/p_".$nome_foto);
							} else {
								$canvas = new canvas();
								$canvas->load_url($file)->resize( "100%" )->save($dir."/p_".$nome_foto);
							}

						}

						$ins_galeria = "INSERT INTO admin_galerias (id_externo,tabela,campo,caminho,ordem) VALUES ('$id','$table','$key','$nome_foto','$ordem_atual')";
						mysqli_query($GLOBALS["db"],$ins_galeria);
					}
				}
			}
		}
	}


	$url_retorno = explode("&msg=",$_POST["url_retorno"]);
	$url_retorno = $url_retorno[0];
	$_SESSION["ultima-pag"] = true;

	$url_retorno = $_SERVER["HTTP_REFERER"];
	if($insert) { $url_retorno .= "&id=$id";  }
	$url_retorno = str_replace("&msg=sucesso","",$url_retorno);
	if($sucesso) {
		//header("location: ".$url_retorno."&msg=sucesso#".$table);
		header("location: ".$url_retorno."&msg=sucesso");
	} else {
		header("location: ".$url_retorno."&msgErro=erro");
	}
}

if($_POST['acao']=="deletar"){
	$sql = "DELETE FROM textos WHERE id=".$_POST['id'];
	$query = mysqli_query($GLOBALS["db"],$sql);
	header("location: index.php?".str_replace("*", "&", $_POST['qs']."&msg=Removido com sucesso"));
}

if($_POST['acao']=="remover_arquivo"){
	$id = $_POST['id'];
	$campo = $_POST['campo'];

	$sql = "UPDATE textos set $campo='' WHERE id='$id'";
	$query = mysqli_query($GLOBALS["db"],$sql);

}

if($_POST['acao']=="replicar"){

	$table = $_POST["table"];
	$table_em_uso = $table."_em_uso";
	$id = $_POST["replicar"];
	$id_original = $_POST["id"];
	$tabela_original = $_POST["table_original"];

	replicar_em_uso($table,$table_em_uso,$id,null,null,$tabela_original,$id_original);

	$url_retorno = $_SERVER["HTTP_REFERER"];
	header("location: ".$url_retorno."&msg=sucesso");
}

function replicar_em_uso($table_origem,$table_destino,$id,$campo_origem,$id_insert,$tabela_original,$id_original) {

	//PEGA COLUNAS DA TABELA DESTINO
	$sql_colunas = "SHOW COLUMNS FROM $table_destino";
	$query_colunas = mysqli_query($GLOBALS["db"],$sql_colunas);
	$dados_colunas = mysqli_fetch_array($query_colunas);
	$possui_ordem = false;

	$colunas = array();
	while($dados_colunas = mysqli_fetch_array($query_colunas))
	{
		$dados_col = $dados_colunas['Field'];

		if($dados_colunas['Field'] == "admin_ordem") { $possui_ordem = true; }

		if($dados_colunas['Field'] != "id" and $dados_colunas['Field'] != "admin_ordem") {
			$colunas[] = $dados_col;
		}
	}

	//INICIA COPIA
	$where = "";
	if($id != null) { $where = "WHERE id = '$id'"; }
	$sql_origem = mysqli_query($GLOBALS["db"], "SELECT * FROM $table_origem $where");

	$total_registros = mysqli_num_rows($sql_origem);
	while($origem = mysqli_fetch_array($sql_origem)) {
		$cols = "";
		$vals = "";

		for($i = 0; $i < sizeof($colunas); $i++) {
			$col = $colunas[$i];
			if(isset($origem[$col])) {
				$cols .= $col.",";
				$vals .= "'".$origem[$col]."',";
			}
		}

		if($id_insert != null) {
			$cols .= $campo_origem.",";
			$vals .= "'".$id_insert."',";
		}

		if($id_original != null and $id != null) {
			$cols .= "id_".$tabela_original.",";
			$vals .= "'".$id_original."',";
		}

		if($possui_ordem) {
			//pega ultima
			$w = "";
			if($id_insert != "") { $w = "WHERE $campo_origem = '$id_insert'"; }
			$getlast = mysqli_query($GLOBALS["db"], "SELECT admin_ordem FROM $table_destino $w ORDER BY admin_ordem DESC");
			if(mysqli_num_rows($getlast) != 0) {
				$last = mysqli_result($getlast,0,'admin_ordem');
			} else {
				$last = -1;
			}
			$cols .= "admin_ordem,";
			$vals .= "'".($last+1)."',";
		}

		$cols = substr($cols,0,-1);
		$vals = substr($vals,0,-1);

		$insert = "INSERT INTO $table_destino ($cols) VALUES ($vals)";

		//echo $insert."<br />";
		if(mysqli_query($GLOBALS["db"],$insert)) {
			$id_insert2 = mysqli_insert_id($GLOBALS["db"]);

			//Busca tabelas externas

			global $dbname;
			$externo_sql = mysqli_query($GLOBALS["db"], "select table_name from information_schema.columns where table_schema = '$dbname' and column_name = 'id_".$table_destino."'") or die(mysqli_error($GLOBALS["db"]));
			if(mysqli_num_rows($externo_sql) != 0) {
				while($externo = mysqli_fetch_array($externo_sql)) {

					$externo_origem_sql = mysqli_query($GLOBALS["db"], "select table_name from information_schema.columns where table_schema = '$dbname' and column_name = 'id_".$table_origem."'") or die(mysqli_error($GLOBALS["db"]));
					if(mysqli_num_rows($externo_origem_sql) != 0) {
	 					while($externo_origem = mysqli_fetch_array($externo_origem_sql)) {

							$tb_dest = str_replace("_em_uso","",$externo["table_name"]);
							if($tb_dest == $externo_origem["table_name"]) {

								$campo_origem = 'id_'.$table_destino;
								replicar_em_uso($externo_origem["table_name"],$externo["table_name"],null,$campo_origem,$id_insert2,null,null);


							}

						}

					}


				}
			}



		}


	}

}


function forma_query($table,$colunas,$id=null) {


	$query = "";
	
	if(in_array("url",$colunas)) {
		$campo_principal = campo_principal($table);
		$url_o = cleanForShortURL($_POST[$campo_principal]);
		$url = $url_o;
		$sql_and = "";
		if($id != null) { $sql_and = " AND id<>'".$_POST['id']."'"; }
 		$sql_url = "SELECT * FROM $table WHERE url='$url' $sql_and";
		$query_url = mysqli_query($GLOBALS["db"],$sql_url);
		$i_url = 1;

		while(mysqli_num_rows($query_url)>0){
			$i_url++;
			$url = $url_o."-$i_url";
			$sql_url = "SELECT * FROM $table WHERE url='$url'";
			$query_url = mysqli_query($GLOBALS["db"],$sql_url);
		}

		$query = "url='$url'";
	}

	foreach($_POST as $key => $value) {

		$tipo = "";
		$sql_config = mysqli_query($GLOBALS["db"], "SELECT * FROM admin_configuracoes WHERE tabela = '$table' and campo = '$key'");

		if(mysqli_num_rows($sql_config) != 0) {
			$tipo = mysqli_result($sql_config,0,"tipo");

			if($tipo == "senha") {
				$value = md5($value);
			}

			if($tipo == "data" || $tipo == "datahora") {
				$value = formata_data_bd($value);
			}

			if($tipo == "dinheiro") {
				$value = str_replace(".","",$value);
				$value = str_replace(",",".",$value);
			}
		}

		if(gettype($value)=="array"){
			$value = implode("|", $value);
		}


		if(in_array($key, $colunas) ){
			if($query!=""){
				$query .= ",";
			}
			$query .= $key."='".addslashes($value)."'";
		}
	}

	$count = 0;
	foreach ($_FILES as $key => $value) {

		if(in_array($key,$colunas)) {

			if(!is_array($value['name'])) { //Se for array, não faz nada (porque arrays são galerias)

				if($value['name']!="") {

					$dir = "uploads/";
					$nome = $value['name'];
					$nome_arr = explode(".", $nome);
					$ext = ".".end($nome_arr);
					$nome_original = cleanForShortURL($nome_arr[0]);
					$nome_foto = $nome_original.$ext;
					$i = 0;
					while(file_exists($dir.$nome_foto)){
						$i++;
						$nome_foto = $nome_original."_".$i.$ext;
					}

					move_uploaded_file($value['tmp_name'], "$dir$nome_foto");

					if(strpos($value['type'], "image")!==false){

						$file = $dir.$nome_foto;
						$t = getimagesize($file);

						if($t[0]>1000){
							$canvas = new canvas();
							$canvas->load_url($file)->resize( 1000 )->save($dir."/g_".$nome_foto);
						} else {
							$canvas = new canvas();
							$canvas->load_url($file)->resize( "100%" )->save($dir."/g_".$nome_foto);
						}

						if($t[0]>500){
							$canvas = new canvas();
							$canvas->load_url($file)->resize( 500 )->save($dir."/m_".$nome_foto);
						} else {
							$canvas = new canvas();
							$canvas->load_url($file)->resize( "100%" )->save($dir."/m_".$nome_foto);
						}

						if($t[0]>100){
							$canvas = new canvas();
							$canvas->load_url($file)->resize( 100 )->save($dir."/p_".$nome_foto);
						} else {
							$canvas = new canvas();
							$canvas->load_url($file)->resize( "100%" )->save($dir."/p_".$nome_foto);
						}

					}

					$query .= ",$key='$nome_foto'";
				}
			}
		}
	}

	if(isset($_POST["id_externo"]) and isset($_POST["table_externo"])) {
		$query .= ",id_".$_POST["table_externo"]."='".$_POST["id_externo"]."'";
	}

	$sql = "";

	if($id != "") {
		$sql = "UPDATE $table SET $query WHERE id = '$id'";
		if($query == "") { $sql = ""; }
	} else {
		$sql = "INSERT INTO $table SET $query";
		if($query == "") { $sql = "INSERT INTO $table SET id = null"; }
	}


	return $sql;


}

?>