<? include("header.php"); ?>

<div class="container login">
  
  <div class="row" id="pwd-container">
    <div class="col-md-4"></div>
    
    <div class="col-md-4">
      <section class="login-form">
        <form method="post" action="login_acoes.php" class='log-form' role="login">
          <img src="../img/logo_admin.png" class="img-responsive" alt="" style="margin: 0px auto;">
          <br />
          <label>
          	<input type="text" required name="usuario" placeholder="Usuário" class="form-control input-lg" value="">
          </label>

          <label>
          	<input type="password" required name="senha" class="form-control input-lg" id="password" placeholder="Senha"><ul class="error-list"></ul>
          </label>
          
          <div class="pwstrength_viewport_progress"><div class="progress"><div class="progress-bar"><span class="password-verdict"></span></div></div></div>
          
       
          <button type="submit" onclick="oioi()" name="go" class="btn btn-lg btn-primary btn-block">Entrar</button>
          
        </form>
      </section>  
      </div>
      
      <div class="col-md-4"></div>

        

  </div>
  
</div>

<? include("footer.php"); ?>

<!--BACKSTRETCH-->
<!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
<script type="text/javascript" src="js/backstretch.min.js"></script>
<script>
    $.backstretch('img/bg-login.jpg', {speed: 500});
</script>

<script>
// function oioi() {
//   alert("Volte sempre!!");
// }
</script>