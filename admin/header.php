<? ob_start(); session_start(); ?>
<!doctype html>
<html class="no-js" lang="pt-br">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Administrador</title>
	<link rel="stylesheet" href="css/jquery-ui.min.css" />
	<link rel="stylesheet" href="css/validador.css" />
	<link rel="stylesheet" href="css/jcrop.min.css" />
	<link rel="stylesheet" href="css/colorpicker.css" />
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/bootstrap-theme.min.css" />
	<link rel="stylesheet" href="css/sweetalert.css" />
	<link rel="stylesheet" href="css/fontpicker.min.css" />
	<link rel="stylesheet" href="css/fontpicker-boostrap.css" />
	<link rel="stylesheet" href="css/material-design-icons.css" />
	<link rel="stylesheet" href="css/fancybox/jquery.fancybox.css" />
	<link rel="stylesheet" href="css/estilo.css" />
	<script src="js/modernizr.js"></script>
	</head>
<body>