<div class="clearfix"></div>
<footer id="contato">
    <section class="footer col-md">
        <div class="container">
            <div class="col-md-11 box_logo">
                    <img src="img/logo_footer.png" alt="" />
            </div>
            <div class="col-md-1 box_social">
                    <ul>
                        <li><a href="<?=$dados_contato["instagram"];?>" target="_blank"><img src="img/logo_footer_instagram.png" alt="Instagram"></a></li>
                        <li><a href="<?=$dados_contato["facebook"];?>" target="_blank"><img src="img/logo_footer_facebook.png" alt="Facebook"></a></li>
                        <li><a href="<?=$dados_contato["linkedin"];?>" target="_blank"><img src="img/logo_footer_linkedin.png" alt="LinkedIn"></a></li>
                        <li><a href="<?=$dados_contato["jusbrasil"];?>" target="_blank"><img src="img/logo_footer_ju.png" alt="JusBrasil"></a></li>
                    </ul>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-11 txt_footer">
                <p>
                    <img src="img/pin_footer.png" alt="">
                    <?=$dados_contato["endereco"];?>
                </p>
                <p><img src="img/tel_footer.png" alt=""><?=$dados_contato["telefone"];?>  <img src="img/carta_footer.png" alt=""><?=$dados_contato["email_destinatario"];?></p>
            </div>
            <div class="col-md-1"></div>
            <div class="clearfix"></div>            
        </div>
    </section>
    <div class="copy">
        <div class="espaco-footer">
            <div class="container">
                <div class="col-md-12 text-footer">
                    <p>
                        © Copyright <?=date("Y");?> – Leroy & Miranda. Todos os direitos reservados. (Alltype Publicidade e Marketing)
                    </p>
                </div> 
                <!-- <div class="col-md-3 col-xs-6 col-sm-3">
                    <a href="http://www.publixcomunicacao.com.br" target="_blank">
                        <img src="img/publix-logo.png" class="img-focus">
                    </a>
                </div> -->
            </div>
        </div>
    </div>
</footer> 
