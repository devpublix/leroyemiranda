(function(){ 
    // ajustes();
    // var wow = new WOW().init();
    var isMobile = window.matchMedia("only screen and (max-width: 767px)");

// ===================== if for mobile version ====================== //
// ===================== if(isMobile.matches) { ===================== //
// ===================== }else{ ===================================== //
// ===================== } ========================================== //
// ===================== if for mobile version ====================== //

jQuery(document).ready(function($) {
    $(".scroll").click(function(event){
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top-50}, 1500);
    });
});

$(document).ready(function() {

    $(".email-envio").validador({
        placeholder: false,
        url: "formulario-envio.php",
        callback: function (data) {
            if ($.trim(data) == "sucesso") {
                $(".email-envio")[0].reset();
                swal("Dados Enviados com Sucesso!", "Em breve entraremos em contato!", "success");
            }
            if ($.trim(data) == "erro") {
                $('.giro').hide();
                $('.enviando').hide();
                swal("Erro. Tente Novamente!", "Erro ao enviar!", "error");
            }
        }
    })

    $(".form-newsletter").validador({
        placeholder: false,
        url: "newsletter-cadastro.php",
        callback: function (data) {
            if ($.trim(data) == "sucesso") {
                $(".form-newsletter")[0].reset();
                swal("Email cadastrado com Sucesso!", "Foi enviado um e-mail de confirmação para o seu endereço de e-mail.", "success");
            }
            if ($.trim(data) == "erro") {
                $('.giro').hide();
                $('.enviando').hide();
                swal("Erro. Tente Novamente!", "Erro ao cadastrar!", "error");
            }
        }
    })
})

})(jQuery);


var isMobile = window.matchMedia("only screen and (max-width: 767px)");

  var swiper = new Swiper('.swiper-container-banner', {
    loop: true,

    pagination: {
      el: '.swiper-pagination',
    },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
  });

    var menu = ['QUEM SOMOS', 'MISSÃO', 'VISÃO', 'VALORES']
    var mySwiper = new Swiper ('.swiper-container-escritorio', {
        // If we need pagination
        loop: true,
        effect: 'fade',
        pagination: {
          el: '.swiper-pagination-escritorio',
                clickable: true,
            renderBullet: function (index, className) {
              return '<span class="' + className + '">' + (menu[index]) + '</span>';
            },
        },
      })

        if (isMobile.matches) {
            var swiper = new Swiper('.swiper-container-equipe', {
              loop: true,
              slidesPerView: 1,
                autoplay: {
                  delay: 2500,
                  disableOnInteraction: false,
                },
              pagination: {
                el: '.swiper-pagination-equipe',
                clickable: true,
              },
            });
        } else {
            var swiper = new Swiper('.swiper-container-equipe', {
              loop: true,
              slidesPerView: 4,
                autoplay: {
                  delay: 2500,
                  disableOnInteraction: false,
                },
                  navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                  },
            });
        }

      var swiper = new Swiper(".mySwiper", {
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
      });

// =========================== MENU-MOBILE================================= //


$('.menu-mobile').click(function () {
    var css_right = $('.logo-mobile').css("left");

    if (css_right == "-500px;") {
        $(".logo-mobile").animate({"left": '0px'});
    } else {
        if (css_right == "0px") {
            $(".logo-mobile").animate({"left": '-500px'});
        }
    }
});

$('.menu-mobile').click(function () {
    var css_right = $('.logo-mobile').css("left");
});

$('.expande-clica').click(function () {
    var id = $(this).attr('id').replace('lista-', '');

    $(".expandido").hide();
    $("#aba-" + id).show();
    $(".abre").slideDown();
})


$(document).ready(function () {
    var id = location.hash.replace('#', '');
    
    if(id!='') {
        $('.area-int').fadeOut('fast', function () {

            $('.a-detalhes').hide();
            $('.area-detalhes' + id).show();
            $('.area-int').fadeIn('fast');
        });

        $('.colorir').removeClass('ativo');
        $('#area-' + id).find('.colorir').addClass('ativo');
    }
})


$('.menu-mobile').click(function () {
    var css_right = $('.logo-mobile').css("left");

    if (css_right == "-500px") {
        $(".logo-mobile").animate({"left": '0px'});
    } else {
        if (css_right == "0px") {
            $(".logo-mobile").animate({"left": '-500px'});
        }
    }
});


// =========================== MENU-MOBILE================================= //