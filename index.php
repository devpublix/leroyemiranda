<?php
    require_once("admin/config.php");
    global $whitelist, $base_url, $ini;

    $iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
    $ipad = strpos($_SERVER['HTTP_USER_AGENT'],"iPad");
    $android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
    $palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
    $berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
    $ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
    $symbian =  strpos($_SERVER['HTTP_USER_AGENT'],"Symbian");

    require_once("admin/funcoes.php"); 

    $sql = mysqli_query($GLOBALS['db'],"SELECT * FROM contato WHERE id=1");
    $dados_contato = mysqli_fetch_assoc($sql);
?>
<!doctype html>
<html lang="pt-en">
<head>
    <title>Leroy & Miranda | Advocacia Patrimonial</title>
    <base href="<?php echo $base_url; ?>">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <!-- ================================ FAVICON ================================ -->
    <!-- ========================================================================= -->
    <link rel="apple-touch-icon" sizes="57x57" href="img/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
    <link rel="manifest" href="img/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- ========================================================================= -->
    <!-- ================================ FAVICON ================================ -->
    <link rel="stylesheet" href="css/validador.css">
    <link rel="stylesheet" href="css/swiper.min.css">
    <link rel="stylesheet" href="js/fancybox/jquery.fancybox.css" />
    <link rel="stylesheet" href="css/jssocials.css">
    <link rel="stylesheet" href="css/bootstrap.3.3.7.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/sweetalert.css">
</head>
<body>

<?php
    $server = explode("/",$_SERVER["REQUEST_URI"]);
    $pag = $server[$ini];

    require_once 'menu.php';

    $server = explode("/",$_SERVER["REQUEST_URI"]);
    $pag = $server[$ini];

    if(strpos($pag,"?") !== false) {
        $pag = explode("?",$pag);
        $pag = $pag[0];
    }

    if($pag == null or $pag == "index.php" or !file_exists($pag.".php")) {
        $pag = "home";
    }
?>

</body>

<?php if(file_exists($pag.".php")) { require_once($pag.".php"); } ?>

<?php require_once 'footer.php';?>

<div id="fb-root"></div>

<script type="text/javascript">
    var base_url = "{{ url('/') }}";
</script>
<script type="text/javascript" src="js/swiper.min.js"></script>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<!-- <script type="text/javascript" src="js/jquery.fancybox.min.js"></script> -->
<script type="text/javascript" src="js/bootstrap.3.3.7.min.js"></script>
<script type="text/javascript" src="js/maskedinput.js"></script>
<script type="text/javascript" src="js/pace.min.js"></script>
<script type="text/javascript" src="js/jssocials.min.js"></script>
<script type="text/javascript" src="js/sweetalert.min.js"></script>
<script type="text/javascript" src="js/jquery.maskedinput-1.1.4.pack.js"/></script>
<script type="text/javascript" src="js/validador.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>


<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#menu-fixo").hide();

    jQuery('a#menu-fixo').click(function () {
         jQuery('body,html').animate({
           scrollTop: 0
         }, 800);
        return false;
    });

    jQuery(window).scroll(function () {
         if (jQuery(this).scrollTop() > 100) {
            jQuery('#menu-fixo').fadeIn();
         } else {
            jQuery('#menu-fixo').fadeOut();
         }
     });
});

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if(d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6&appId=1718657101724003";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

</html>