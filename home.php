<?php

	$pagina = $subpagina = null;

	//Buscando Página e Subpágina
	$pagina = $pag;
	$subpagina = $server[$ini+1];
?>
<!-- conteudo da home -->

<div class="col-md-12 banner_home">

	<div class="container central_logo">
		<img src="img/logo_branca_leroy.png" alt="Leroy e Miranda Advocacia Patrimonial"><br/>		
	</div>
  

  <div class="swiper-container-banner">
    <div class="swiper-wrapper">

    <?
      $sql = mysqli_query($GLOBALS['db'],"SELECT * FROM banner ORDER BY admin_ordem");
    ?>
    <? while ($dados = mysqli_fetch_assoc($sql)) {?>
      <div class="swiper-slide">
			<div class="container">
				<div class="center_box">
					<h1 id="qs"	>
						<span><?=$dados["texto01"];?></span>
					</h1>
				</div>
			</div>
		</div>	
	<?}?>

    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
    <div class="swiper-button-next swiper-button-white"></div>
    <div class="swiper-button-prev swiper-button-white"></div>
  </div>



</div>


<div class="col-md-12 quemsomos_home" >

<!-- Slider main container -->
<div class="swiper-container-escritorio">
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper">

    <?
      $sql = mysqli_query($GLOBALS['db'],"SELECT * FROM oescritorio ORDER BY admin_ordem");
    ?>
    <? while ($dados = mysqli_fetch_assoc($sql)) {?>

        <!-- Slides -->
        <div class="swiper-slide">

			<div class="container" >

				<div class="col-md-4 icon_logo">
					<img src="img/logo_icon.jpg" alt="" id="escritorio" >	
				</div>

				<div class="col-md-8 conteudo_quemsomso" >
					<h2>O ESCRITÓRIO </h2>
					<h3><?=$dados["titulo"];?></h3>

					<?=$dados["texto"];?>

					<!-- If we need pagination -->
					<div class="swiper-pagination-escritorio"></div>
				</div>
				<div class="clearfix"></div>  

				

			</div>

		</div>

	<?}?>

    </div>

</div>

</div>



<div class="col-md-12 adv_patrimonial_home">
	<div class="container">

		<h2>
			ADVOCACIA<br/>
			<span>PATRIMONIAL</span>
		</h2>

		<?php  if ($ipad == true){}else if ($iphone || $android || $palmpre || $ipod || $berry || $symbian == true){?>

		<div class="center_img">
			<a href="advocacia-patrimonial">
				<img src="img/linha_tempo.jpg" alt="" class="img-responsive" />
			</a>
		</div>
	<? } else { ?>
		<div class="linha_tempo">
			<a href="advocacia-patrimonial/constituicao-de-patrimonio" class="bolinha01">
				<img src="img/m_constituicao-do-patrimonio.png" onMouseOver="this.src='img/m_constituicao-do-patrimonio--1.png'" onMouseOut="this.src='img/m_constituicao-do-patrimonio.png'" alt="" />
			</a>
			<a href="advocacia-patrimonial/recuperacao-do-patrimonio" class="bolinha02">
				<img src="img/m_recuperacao-do-patrimonio.png" onMouseOver="this.src='img/m_recuperacao-do-patrimonio--1.png'" onMouseOut="this.src='img/m_recuperacao-do-patrimonio.png'" alt="" />
			</a>
			<a href="advocacia-patrimonial/manutencao-do-patrimonio" class="bolinha03">
				<img src="img/m_manutencao-do-patrimonio.png" onMouseOver="this.src='img/m_manutencao-do-patrimonio--1.png'" onMouseOut="this.src='img/m_manutencao-do-patrimonio.png'" alt="" />
			</a>
			<a href="advocacia-patrimonial/patrimonio-em-crise" class="bolinha04">
				<img src="img/m_patrimonio-em-crise.png" onMouseOver="this.src='img/m_patrimonio-em-crise--1.png'" onMouseOut="this.src='img/m_patrimonio-em-crise.png'" alt="" />
			</a>
		</div>		

		<div class="center_img">
			<a href="advocacia-patrimonial">
				<img src="img/img_patrimonial_home.jpg" alt="" class="img-responsive" />
			</a>
		</div>
	<? }  ?>
	</div>
</div>

<div class="col-md-12 area_atuacao_home" id="atuacao">
	<div class="container">

		<h2>
			ÁREAS<br/>
			<span> DE ATUAÇÃO</span>
		</h2>


		<div class="icons">

		    <?
		      $sql = mysqli_query($GLOBALS['db'],"SELECT * FROM itens_atuacao ORDER BY admin_ordem");
		    ?>
		    <? while ($dados = mysqli_fetch_assoc($sql)) {?>
			<div class="col-md-3 icon">
				<a href="atuacao/<?=$dados["url"];?>">
					<img src="admin/uploads/<?=$dados["img01"];?>" alt="" onMouseOver="this.src='admin/uploads/<?=$dados["img02"];?>'" onMouseOut="this.src='admin/uploads/<?=$dados["img01"];?>'" />
					<?=$dados["item"];?>
				</a>
			</div>
			<?}?>

		</div>

	</div>
</div>


<?
	// Buscando dados Gerais
	$sql = mysqli_query($GLOBALS['db'],"SELECT * FROM onde_atuamos WHERE id=1");
	$dados = mysqli_fetch_assoc($sql);
?>
<div class="col-md-12 onde_atuamos_home" >
	<div class="container">

		<h2>
			ONDE<br/>
			<span>ATUAMOS</span>
		</h2>

		<div class="col-md-6 nopadding abrangencia">

			<h3>ABRANGÊNCIA NACIONAL</h3>

			<div class="box">
				<?=$dados["texto01"];?>
			</div>
			
		</div>


		<div class="col-md-6 ">
			<img src="admin/uploads/<?=$dados["img01"];?>" onMouseOver="this.src='admin/uploads/<?=$dados["img02"];?>'" onMouseOut="this.src='admin/uploads/<?=$dados["img01"];?>'" alt="" class="img-responsive" />	
		</div>

	</div>
</div>



<div class="col-md-12 nosso_time" id="equipe">
	<div class="container">

		<h2>
			NOSSO<br/>
			<span> TIME</span>
		</h2>
		<div class="clearfix"></div>

<?php  if ($ipad == true){}else if ($iphone || $android || $palmpre || $ipod || $berry || $symbian == true){?>
	<br/><br/>
		<div class="clearfix"></div>

	    <div class="swiper-container-mobile mySwiper">
	      <div class="swiper-wrapper">

		    <?
				$query = "
				SELECT
				equipe.id,
				equipe.nome,
				equipe.formacao,
				equipe.idioma,
				equipe.texto,
				equipe.email,
				equipe.linkedin,
				equipe.img_home,
				equipe.img,
				equipe.url,
				equipe.admin_ordem,
				equipe.id_equipe_categproa,
				equipe_categproa.categoria,
				equipe_categproa.cor
				FROM
				equipe
				left JOIN equipe_categproa ON equipe.id_equipe_categproa = equipe_categproa.id
				ORDER BY admin_ordem
				";

		      $sql = mysqli_query($GLOBALS['db'],$query);
		    ?>
		    <? while ($dados = mysqli_fetch_assoc($sql)) {?>
	        <div class="swiper-slide" style="wisth: 256px;">

	        	<div class="<?=$dados["cor"];?>" style="margin: 0px !important;">
					<a href="equipe/<?=$dados["url"];?>">
						<img src="admin/uploads/<?=$dados["img_home"];?>" alt="" />
					</a><br/>
					
					<h3><?=$dados["nome"];?></h3>

					<a href="mailto:<?=$dados["email"];?>" class="carta">&nbsp;</a>
					<a href="<?=$dados["linkedin"];?>" target="_blank" class="linkedin">&nbsp;</a>
	        	</div>

	        </div>
			<?}?>
	      </div>
	      <div class="swiper-button-next swiper-button-white" style="margin-top: 30ppx"></div>
	      <div class="swiper-button-prev swiper-button-white" style="margin-top: 30ppx"></div>
	    </div>


<?}else {?>

		<div class="swiper-container-equipe equipe">
			 <div class="swiper-wrapper">
			    <?
					$query = "
					SELECT
					equipe.id,
					equipe.nome,
					equipe.formacao,
					equipe.idioma,
					equipe.texto,
					equipe.email,
					equipe.linkedin,
					equipe.img_home,
					equipe.img,
					equipe.url,
					equipe.admin_ordem,
					equipe.id_equipe_categproa,
					equipe_categproa.categoria,
					equipe_categproa.cor
					FROM
					equipe
					left JOIN equipe_categproa ON equipe.id_equipe_categproa = equipe_categproa.id
					ORDER BY admin_ordem
					";

			      $sql = mysqli_query($GLOBALS['db'],$query);
			    ?>
			    <? while ($dados = mysqli_fetch_assoc($sql)) {?>
				<div class="swiper-slide <?=$dados["cor"];?>">
					<a href="equipe/<?=$dados["url"];?>">
						<img src="admin/uploads/<?=$dados["img_home"];?>" alt="" />
					</a><br/>
					
					<h3><?=$dados["nome"];?></h3>

					<a href="mailto:<?=$dados["email"];?>" class="carta">&nbsp;</a>
					<a href="<?=$dados["linkedin"];?>" target="_blank" class="linkedin">&nbsp;</a>
				</div>
				<?}?>
			</div>
		    <div class="swiper-button-next swiper-button-white"></div>
		    <div class="swiper-button-prev swiper-button-white"></div>
		</div>

<?}?>

	</div>
</div>






<div class="col-md-12 adv_patrimonial_home">
	<div class="container">

		<h2>
			ÚLTIMAS<br/>
			<span>PUBLICAÇÕES</span>
		</h2>
 
	    <?
	      $sql = mysqli_query($GLOBALS['db'],"SELECT * FROM publicacoes ORDER BY admin_ordem");
	    ?>
	    <? while ($dados = mysqli_fetch_assoc($sql)) {?>
		<div class="col-md-2" style="display: none;">
			<a href="admin/uploads/<?=$dados["arquivo"];?>">
				<img src="admin/uploads/<?=$dados["img"];?>" alt="<?=$dados["nome"];?>" />
			</a>
		</div>
		<?}?>

		<script src="https://apps.elfsight.com/p/platform.js" defer></script>
		<div class="elfsight-app-8878b8e4-d9a6-4018-ac43-686c38d9cd0a"></div>

	</div>
</div>