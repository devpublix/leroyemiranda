<?php  if ($ipad == true){}else if ($iphone || $android || $palmpre || $ipod || $berry || $symbian == true){?>
<div class="top-mob">    
    <a href="home">
        <img src="img/logo.png" class="img-responsive" style="display: inline-block; width: 23%;">
    </a>
    <div class="traco-mobile"></div>
</div>
<div class="clearfix"></div>
    
<div class="menu-mobile">
    <div class="logo-mobile">
        <div class="col-xs-12 nopadding">
            <div class="espaco">
                <div class="col-xs-2 nopadding">
                    <img src="img/fecha.png" style="float: left; width: 54px; padding: 15px;">
                </div>
                <div class="col-xs-8 nopadding">
                    <a href="home">
                        <img src="img/logo.png">
                    </a> 
                </div>
                <div class="col-xs-2 nopadding"></div>
            </div>
        </div>
        <div class="clearfix"></div>
        <nav class="menu-mobile-itens">
            <div class="itens">
                <nav>
                    <ul style="position: static;">
                            <li><a href="escritorio">O ESCRITÓRIO</a></li>
                            <li><a href="advocacia-patrimonial/constituicao-de-patrimonio">ADVOCACIA PATRIMONIAL</a></li>
                            <li><a href="atuacao/contencioso-de-grande-repercussao">ATUAÇÃO</a></li>
                            <li><a href="nosso-time">NOSSO TIME</a></li>
                            <li><a href="contato" >CONTATO</a></li>
                        <li style="text-align: center !important;">
                            <a href="<?=$dados_contato["maps"];?>" target="_blank">
                                <img src="img/icon_pin.png" >
                            </a>
                            <a href="mailto:<?=$dados_contato["email_destinatario"];?>">
                                <img src="img/icon_email.png" ></a> 
                            <a href="<?=$dados_contato["telefone"];?>" target="_blank"><img src="img/icon_tel.png" ></a> 
                            <a href="<?=$dados_contato["instagram"];?>" target="_blank"><img src="img/icon_instagram.png"></a> 
                            <a href="<?=$dados_contato["facebook"];?>" target="_blank"><img src="img/icon_facebook.png"></a>
                            <a href="<?=$dados_contato["linkedin"];?>" target="_blank"><img src="img/icon_linkedin.png" ></a> 
                        </li>
                    </ul>
                </nav>
            </div>
        </nav>
    </div>
</div>

<!-- =======================================DESKTOP=========================================== -->
   
<?php }else{?>

<div id="menu-fixo" class="menu-fixo">
    <div class="topo">
        <div class="col-md-12">
            <div class="container">
                <div class="col-md-2 col-sm-2">
                    <div class="logo">
                        <a href="home">
                            <img src="img/logo.png" class="img-responsive logo-menu">
                        </a>
                    </div>
                </div>
                <div class="col-md-2 barra-icons">
                    <a href="<?=$dados_contato["maps"];?>" target="_blank"><img src="img/icon_pin.png" class="img-responsive"></a>
                    <a href="mailto:<?=$dados_contato["email_destinatario"];?>"><img src="img/icon_email.png" class="img-responsive"></a> 
                    <a href="<?=$dados_contato["telefone"];?>" target="_blank"><img src="img/icon_tel.png" class="img-responsive"></a> 
                    <a href="<?=$dados_contato["instagram"];?>" target="_blank"><img src="img/icon_instagram.png" class="img-responsive"></a> 
                    <a href="<?=$dados_contato["facebook"];?>" target="_blank"><img src="img/icon_facebook.png" class="img-responsive"></a> 
                    <a href="<?=$dados_contato["linkedin"];?>" target="_blank"><img src="img/icon_linkedin.png" class="img-responsive"></a> 
                </div>
                <div class="col-md-8 col-sm-8">
                    <div class="menu">
                        <ul class="fix">
                            <li><a  href="escritorio">O ESCRITÓRIO</a></li>
                            <li><a href="advocacia-patrimonial/constituicao-de-patrimonio">ADVOCACIA PATRIMONIAL</a></li>
                            <li><a href="atuacao/contencioso-de-grande-repercussao">ATUAÇÃO</a></li>
                            <li><a href="nosso-time">NOSSO TIME</a></li>
                            <li><a href="contato" >CONTATO</a></li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>  
            </div>
        </div>
    </div>
</div>

<section class="menu">
    <div class="col-md-12">
        <div class="container">
            <div class="col-md-12 altura-menu">
                <div class="col-md-2">
                    <a href="home"><img src="img/logo.png" class="img-responsive logo-menu"></a>
                </div>
                <div class="col-md-2 barra-icons">
                    <a href="<?=$dados_contato["maps"];?>" target="_blank"><img src="img/icon_pin.png" class="img-responsive"></a>       
                    <a href="mailto:<?=$dados_contato["email_destinatario"];?>"><img src="img/icon_email.png" class="img-responsive"></a> 
                    <a href="<?=$dados_contato["telefone"];?>" target="_blank"><img src="img/icon_tel.png" class="img-responsive"></a> 
                    <a href="<?=$dados_contato["instagram"];?>" target="_blank"><img src="img/icon_instagram.png" class="img-responsive"></a> 
                    <a href="<?=$dados_contato["facebook"];?>" target="_blank"><img src="img/icon_facebook.png" class="img-responsive"></a> 
                    <a href="<?=$dados_contato["linkedin"];?>" target="_blank"><img src="img/icon_linkedin.png" class="img-responsive"></a> 
                </div>
                <div class="col-md-8 nopadding">
                    <ul class="menu-conteudo">
                        <li><a href="escritorio">O ESCRITÓRIO</a></li>
                        <li><a href="advocacia-patrimonial/constituicao-de-patrimonio">ADVOCACIA PATRIMONIAL</a></li>
                        <li><a href="atuacao/contencioso-de-grande-repercussao">ATUAÇÃO</a></li>
                        <li><a href="nosso-time" >NOSSO TIME</a></li>
                        <li><a href="contato">CONTATO</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>
<? } ?>
<div class="clearfix"></div>